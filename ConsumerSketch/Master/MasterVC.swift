//
//  MasterVC.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit


class MasterVC : UIViewController {
    
    
    var imageView = UIImageView()
    var appcolor = #colorLiteral(red: 0.279621128, green: 0.719354587, blue: 0.9093676028, alpha: 1)

    let mainStory = UIStoryboard.init(name: "Main", bundle: nil)


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jeremyGif = UIImage.gifImageWithName("loading")
        imageView = UIImageView(image: jeremyGif)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 20
        imageView.backgroundColor = UIColor.white
        imageView.clipsToBounds = true
        
    }
    

    func showSpinner(){
        view.isUserInteractionEnabled = false
        view.addSubview(imageView)
        
        imageView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 70).isActive = true
    }
    
    
    func hideSpinner(){
        view.isUserInteractionEnabled = true
        imageView.removeFromSuperview()
    }
    
        func setBorder(view : Any , width : CGFloat,color : UIColor) {
            
            if #available(iOS 13.0, *) {
                (view as AnyObject).layer?.masksToBounds = false
                (view as AnyObject).layer?.borderWidth = width
                (view as AnyObject).layer?.borderColor = color.cgColor

            }
            else {
//                 Fallback on earlier versions
            }
            
        }
    func popUpAlert(title: String,message: String,actiontitle: [String],actionstyle: [UIAlertAction.Style],action: [(UIAlertAction) -> Void ] ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for(index,title) in actiontitle.enumerated() {
            let action = UIAlertAction(title: title, style: actionstyle[index], handler: action[index])
            
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
        
    }
}
