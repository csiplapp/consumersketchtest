//
//  UserModel.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import Foundation

// MARK: - Result
class UserModel{
    
    let id : Int?
    let name : String?
    let username : String?
    let email : String?
    let phone : String?
    let website : String?
    let address: [String: Any]?
    let company: [CompanyModel]?
    
    init(response: [String: Any]) {
        self.id = response["id"] as? Int
        self.name = response["name"] as? String
        self.username = response["username"] as? String
        self.email = response["email"] as? String
        self.phone = response["phone"] as? String
        self.website = response["website"] as? String
        self.address = response["address"] as? [String : Any]
        self.company = response["company"] as? [CompanyModel]

    }

}

class AddressModel: Decodable {
        
    let street : String?
    let suite : String?
    let city : String?
    let zipcode : String?
    
    init(response: [String: Any]) {
        self.street = response["street"] as? String
        self.suite = response["suite"] as? String
        self.city = response["city"] as? String
        self.zipcode = response["zipcode"] as? String

    }
        
}

class CompanyModel: Decodable {
        
    let name : Int?
    let catchPhrase : String?
    let bc : String?
        
}


