//
//  CommentModel.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import Foundation

// MARK: - Result
class CommentModel: Decodable {
    
    let id : Int?
    let postId : Int?
    let name : String?
    let email : String?
    let body : String?
    
    init(response: [String: Any]) {
        
        self.id = response["id"] as? Int
        self.postId = response["postId"] as? Int
        self.name = response["name"] as? String
        self.email = response["email"] as? String
        self.body = response["body"] as? String


    }

}
