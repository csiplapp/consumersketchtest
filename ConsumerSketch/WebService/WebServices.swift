//
//  WebServices.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireMapper
import UIKit

class WebService: NSObject {
    
    static let baseurl = "http://jsonplaceholder.typicode.com/"
    
    static func errorCondition(error : Error) -> NSDictionary{
        
        switch error._code {
        case -1001:
            var result = [String: String]()
            result["status"] = "1001"
            result["response"] = "server connection time out"
            return(result as NSDictionary)
        case -1004:
            var result = [String: String]()
            result["status"] = "1004"
            result["response"] = "server connection failed"
            return(result as NSDictionary)
        case -1005:
            var result = [String: String]()
            result["status"] = "1005"
            result["response"] = "The network connection was lost"
            return(result as NSDictionary)
        case -1009:
            var result = [String: String]()
            result["status"] = "1009"
            result["response"] = "The Internet connection appears to be offline."
            return(result as NSDictionary)
        default:
            var result = [String: String]()
            result["status"] = "0007"
            result["response"] = "Server not responding, please try again."
            return(result as NSDictionary)
        }
    }
    
    // Get Posts API Call
    class func getPost(completionHandler:@escaping (_ result:NSArray)->())
    {
    
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            Alamofire.request(baseurl+"posts", method: .get,encoding: JSONEncoding.default).responseJSON { (response) in
                switch response.result {
                case .success:
                    print("Validation Successful")
                    if let result = response.result.value as? [[String: Any]]{
                        DispatchQueue.main.async {
                            completionHandler(result as NSArray)

                        }
                    }
                case .failure(let error):

                    completionHandler(["Error"])
                    print("\n\nAuth request failed with error:\n \(error)")


                }

            }
        }
    }
    
    
    // Get User list API Call
    class func getUser(completionHandler:@escaping (_ result:NSArray)->())
    {
                        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            Alamofire.request(baseurl+"users", method: .get,encoding: JSONEncoding.default).responseJSON { (response) in
            
                switch response.result {
                
                case .success:
                
                    print("Validation Successful")
                    if let result = response.result.value as? [[String: Any]]{

                        DispatchQueue.main.async {
                            completionHandler(result as NSArray)

                        }
                    }
                case .failure(let error):

                    completionHandler(["Error"])
                    print("\n\nAuth request failed with error:\n \(error)")


                }

            }
        }
    }
    
    // Get Post Comments API Call
    class func getComments(completionHandler:@escaping (_ result:NSArray)->())
    {
                        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
            Alamofire.request(baseurl+"comments", method: .get,encoding: JSONEncoding.default).responseJSON { (response) in
            
                switch response.result {
                
                case .success:
                
                    print("Validation Successful")
                    if let result = response.result.value as? [[String: Any]]{

                        DispatchQueue.main.async {
                            completionHandler(result as NSArray)

                        }
                    }
                case .failure(let error):

                    completionHandler(["Error"])
                    print("\n\nAuth request failed with error:\n \(error)")


                }

            }
        }
    }
    
}
