//
//  PostDetailVC.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit
import CoreData

class PostDetailVC : MasterVC {
    
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnUser: UIButton!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var lblView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let coredatamanager = CoreDataManager.shared()

    var strPostTitle = ""
    var strPostDesc = ""
    var screenType = "user"
    var userid = 1
    var postId = 1
    var arrUserData = [UserViewModel]()
    var arrAllComments = [Comments]()
    var arrCommentData = [CommentViewModel]()

    var arrUsers = [Users]()
    var arrComments = [Comments]()
    
    //View lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpUI()

        //Check internet connection available if yes then fetch data from Api else fetch records from local storage
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            if arrUsers.count > 0 {
                fetchUsersFromLocalStorage()
            }else{
                showSpinner()
                getallUsersApiCall()

            }
        }else{
            print("Internet Connection not Available!")
            fetchUsersFromLocalStorage()

        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //Set up UI controls for screen
    func setUpUI() {
        
        postTitle.text = strPostTitle
        desc.text = strPostDesc.replacingOccurrences(of: "\n", with: "")
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        mainView.backgroundColor = appcolor
        lblView.backgroundColor = appcolor
        
        btnUser.backgroundColor = appcolor
        btnComment.backgroundColor = UIColor.white
        
        btnComment.setTitleColor(appcolor, for: .normal)
        btnUser.setTitleColor(UIColor.white, for: .normal)
        
        logo.layer.cornerRadius = self.logo.frame.width / 2
        
        btnUser.layer.cornerRadius = self.btnUser.frame.height / 2
        
        btnComment.layer.cornerRadius = self.btnComment.frame.height / 2
        setBorder(view: btnComment as Any, width: 1, color: appcolor)
    }
    
    //MARK:- Button actions
    @IBAction func backClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnUserClicked(_ sender: UIButton) {
        
        btnComment.backgroundColor = UIColor.white
        btnUser.backgroundColor = appcolor
        btnComment.setTitleColor(appcolor, for: .normal)
        btnUser.setTitleColor(UIColor.white, for: .normal)
                
        btnUser.layer.cornerRadius = self.btnUser.frame.height / 2
        
        btnComment.layer.cornerRadius = self.btnComment.frame.height / 2
        
        setBorder(view: btnUser as Any, width: 0, color: appcolor)
        setBorder(view: btnComment as Any, width: 1, color: appcolor)

        screenType = "user"
        
        //Check internet connection available if yes then fetch data from Api else fetch from local storage
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            if arrUsers.count > 0 {
                fetchUsersFromLocalStorage()
            }else{
                showSpinner()
                getallUsersApiCall()

            }
        }else{
            print("Internet Connection not Available!")
            fetchUsersFromLocalStorage()

        }
    }
    
    @IBAction func btnCommentClicked(_ sender: UIButton) {
        
        setBorder(view: btnUser as Any, width: 1, color: appcolor)
        setBorder(view: btnComment as Any, width: 0, color: appcolor)

        btnUser.backgroundColor = UIColor.white
        btnComment.backgroundColor = appcolor
        
        btnUser.setTitleColor(appcolor, for: .normal)
        btnComment.setTitleColor(UIColor.white, for: .normal)
                
        btnComment.layer.cornerRadius = self.btnUser.frame.height / 2
        
        btnUser.layer.cornerRadius = self.btnComment.frame.height / 2

        screenType = "comment"
        
        //Check internet connection available if yes then fetch data from Api else fetch from local storage
        if Reachability.isConnectedToNetwork(){
            if arrComments.count > 0 {
                fetchCommetsFromLocalStorage()
            }else{
                showSpinner()
                getallCommentsApiCall()

            }
        }else{
            
            fetchCommetsFromLocalStorage()

        }
        
    }
    
    
    // Get user list function using API calling on a network connected.
    func getallUsersApiCall(){
        
        WebService.getUser { (result) in
            self.hideSpinner()
            self.arrUserData.removeAll()
            if result.count > 0 {
                            
                for (_, object) in result.enumerated(){

                    self.arrUserData.append(UserViewModel(user: UserModel(response: object as! [String : Any])))
                                
                }

                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }else{
                
                self.popUpAlert(title: "No Post Found", message: "", actiontitle: ["OK"], actionstyle: [.default], action: [{ok in
                                
                    }])
            }
        }
    }
    
 
  // Get comments function using API calling on a network connected.
    func getallCommentsApiCall(){
        
        WebService.getComments { (result) in
            self.hideSpinner()
            self.arrCommentData.removeAll()
            if result.count > 0 {
                  
                
                for (_, object) in result.enumerated(){
                    
                    let  objetTemp = object as? [String : Any]
                    
                    if self.postId == objetTemp?["postId"] as? Int {
                        self.arrCommentData.append(CommentViewModel(comment: CommentModel(response: object as! [String : Any])))

                    }

                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }

            }else{
                
                self.popUpAlert(title: "No Post Found", message: "", actiontitle: ["OK"], actionstyle: [.default], action: [{ok in
                                
                    }])
            }
        }
    }
    
    
    // Get user list function using localstorage on offline.
    func fetchUsersFromLocalStorage(){
        do {
            
            // as? [Users] ?? []

            let userFetchRequest = NSFetchRequest<Users>(entityName: "Users")
            
            userFetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

            let result = try coredatamanager.managedObjectContext.fetch(userFetchRequest)
                        
            self.arrUsers = result
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } catch let error{
            print("error \(error.localizedDescription)")
        }
        
    }
    
    
    // Get comments list function using localstorage on offline.
    func fetchCommetsFromLocalStorage(){
        do {
          
            self.arrComments.removeAll()
            let commentFetchRequest = NSFetchRequest<Comments>(entityName: "Comments")
            
            commentFetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]

            let result = try coredatamanager.managedObjectContext.fetch(commentFetchRequest)

            self.arrAllComments = result
//                as? [Comments] ?? []

            
            if self.arrAllComments.count > 0 {
                for i in 0...self.arrAllComments.count-1 {

                    if self.postId == Int(self.arrAllComments[i].postId) {
                        self.arrComments.append(self.arrAllComments[i])

                    }
                }
            }
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } catch let error{
            print("error \(error.localizedDescription)")
        }
        
    }
    
}

//Post Details delegates and datasorce method
extension PostDetailVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if screenType == "user"{
            if arrUsers.count > 0{
                return arrUsers.count

            }else{
                return arrUserData.count

            }

        }else{
            if arrComments.count > 0 {
                return arrComments.count

            }else{
                return arrCommentData.count

            }

        }
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if screenType == "user"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "usercell", for: indexPath) as! UserCell
            
            if arrUsers.count > 0 {
                let user = self.arrUsers[indexPath.row]
                
                cell.lblName.text = user.name
                cell.lblUserNAme.text = user.username
                cell.lblEmail.text = user.email
                cell.lblPhone.text = user.phone
                cell.btnWebsite.setTitle(user.website, for: .normal)
                cell.lblAddress.text = user.address

            }else{
                let user = self.arrUserData[indexPath.row]
                
                cell.lblName.text = user.name
                cell.lblUserNAme.text = user.username
                cell.lblEmail.text = user.email
                cell.lblPhone.text = user.phone
                cell.btnWebsite.setTitle(user.website, for: .normal)
                let street = user.address?["street"] as? String
                let suite = user.address?["suite"] as? String

                cell.lblAddress.text = "\(street ?? "") \(suite ?? "")"
                
            }
                                                                                                  
            return cell

        }else{
            var postComment = ""
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "commentcell", for: indexPath) as! CommentCell
            
            if arrComments.count > 0 {
                let comment = self.arrComments[indexPath.row]
                postComment = comment.body?.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil) ?? ""

                cell.lblEmail.text = comment.email
                cell.lblDesc.text = postComment
                cell.lblTitle.text = comment.name
    
                
                let approximatewidth = collectionView.frame.width
                let size = CGSize(width: approximatewidth, height: 400)
                let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]
                let estimateFrame = NSString(string: comment.body ?? "Feeling happy").boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
                cell.lbDescHeight.constant = estimateFrame.height + 20

            }else{
                let comment = self.arrCommentData[indexPath.row]
                 postComment = comment.body?.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil) ?? ""
                cell.lblEmail.text = comment.email
                cell.lblDesc.text = postComment
                cell.lblTitle.text = comment.name
                
                
                let approximatewidth = collectionView.frame.width
                let size = CGSize(width: approximatewidth, height: 400)
                let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]
                let estimateFrame = NSString(string: comment.body ?? "Feeling happy").boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
                cell.lbDescHeight.constant = estimateFrame.height + 20

            }
            return cell

        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                
        if screenType == "user"{
            return CGSize(width: self.collectionView.frame.width, height: 200)

        }else{
            
            if arrComments.count > 0 {
                let comment = self.arrComments[indexPath.row]

                let approximatewidth = collectionView.frame.width
                let size = CGSize(width: approximatewidth, height: 400)
                let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize:16)]
                let estimateFrame = NSString(string: comment.body ?? "Feeling happy").boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
                
                return CGSize(width: self.collectionView.frame.width, height: estimateFrame.height + 150)

            }else{
                let comment = self.arrCommentData[indexPath.row]

                let approximatewidth = collectionView.frame.width
                let size = CGSize(width: approximatewidth, height: 400)
                let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]
                let estimateFrame = NSString(string: comment.body ?? "Feeling happy").boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
                
                return CGSize(width: self.collectionView.frame.width, height: estimateFrame.height + 150)

            }

        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }


}
