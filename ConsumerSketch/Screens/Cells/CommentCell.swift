//
//  CommentCell.swift
//  ConsumerSketch
//
//  Created by Tarun Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class CommentCell: UICollectionViewCell {
    
    @IBOutlet weak var lbDescHeight: NSLayoutConstraint!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
