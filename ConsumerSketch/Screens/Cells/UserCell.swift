//
//  UserCell.swift
//  ConsumerSketch
//
//  Created by Tarun Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class UserCell: UICollectionViewCell {
    
    
    @IBOutlet weak var btnWebsite: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblUserNAme: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
