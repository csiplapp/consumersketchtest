//
//  PostCell.swift
//  ConsumerSketch
//
//  Created by Tarun Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class PostCell: UICollectionViewCell {
    
    @IBOutlet weak var titleHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgPost: UIImageView!
    
    override func awakeFromNib(){
        super.awakeFromNib()
        
    }
}
