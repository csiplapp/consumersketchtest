//
//  PostListVC.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit
import CoreData

class PostListVC : MasterVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    
    var arrPosts = [Posts]()

    var arrPostdata = [PostViewModel]()
    var collectionViewFlowLayout = UICollectionViewFlowLayout()

    var arrUserData = [UserViewModel]()
    var arrCommentData = [CommentViewModel]()

    let coredatamanager = CoreDataManager.shared()
    override func viewDidLoad() {
        
        //View lifecycle methods
        super.viewDidLoad()
        mainView.backgroundColor = appcolor
        collectionView.delegate = self
        collectionView.dataSource = self

        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            if arrPosts.count > 0 {
                fetchPosts()
            }else{
                showSpinner()
                getPostListApiCall()

            }
        }else{
            print("Internet Connection not Available!")
            fetchPosts()

        }
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = true

    }
    
    // Get post list function using API calling on a network connected.
    func getPostListApiCall(){
        
        WebService.getPost { (result) in

            if result.count > 0 {
                
                for (_, object) in result.enumerated(){
                    self.arrPostdata.append(PostViewModel(post: PostModel(response: object as! [String : Any])))
                    
                }

                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                self.addPosts()

            }else{
                self.hideSpinner()
                self.popUpAlert(title: "No Post Found", message: "", actiontitle: ["OK"], actionstyle: [.default], action: [{ok in
                    
                    }])
            }
        }
    }
    
    // Add post list in local storage on offline.
    func addPosts(){

        coredatamanager.resetAllRecords(in: "Posts")

        if self.arrPosts.count == 0 {
            self.coredatamanager.preparePostList(dataForSaving: self.arrPostdata)

        }
        
        self.getallUsersApiCall()
                      
    }
    
    // Get post list function using local storage on offline.
    func fetchPosts(){
        do {
            
            let postFetchRequest = NSFetchRequest<Posts>(entityName: "Posts")
            
            postFetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]


            let result = try coredatamanager.managedObjectContext.fetch(postFetchRequest)

            self.arrPosts = result as? [Posts] ?? []
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        } catch let error{
            print("error \(error.localizedDescription)")
        }
        
    }
    
    // Get all users from API on a network connected.
    func getallUsersApiCall(){
        
        WebService.getUser { (result) in
            
            self.arrUserData.removeAll()
            if result.count > 0 {
                
                for (_, object) in result.enumerated(){
                    
                    self.arrUserData.append(UserViewModel(user: UserModel(response: object as! [String : Any])))
                    
                }
                
                self.addUserInLocalStorage()
            }
        }
    }
      
      
    // Get comments function using API calling on a network connected.
      func getallComments(){
          
          WebService.getComments { (result) in

              if result.count > 0 {
                    
                  
                  for (_, object) in result.enumerated(){
                                            
                        self.arrCommentData.append(CommentViewModel(comment: CommentModel(response: object as! [String : Any])))

                  }
      
                  self.addCommentsInLocalStorage()

              }
          }
    }
    
    // Add user data in local storage on offline.
    func addUserInLocalStorage(){
        print("Add users")
        coredatamanager.resetAllRecords(in: "Users")
        
        DispatchQueue.main.async {
            self.coredatamanager.prepareUserList(dataForSaving: self.arrUserData)
        }
        
        getallComments()

    }
    
    // Add comments data in local storage on offline.
    func addCommentsInLocalStorage(){
        print("Add comment")
        
        coredatamanager.resetAllRecords(in: "Comments")
        
        DispatchQueue.main.async {
            self.coredatamanager.prepareCommentList(dataForSaving: self.arrCommentData)
        }
        self.hideSpinner()

    }


}

// Post List delegates and datasorce method
extension PostListVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if arrPosts.count > 0 {
            return arrPosts.count

        }else{
            return arrPostdata.count
        }

    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var postBody = ""
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postcell", for: indexPath) as! PostCell
        cell.imgPost.layer.cornerRadius = 25
        if arrPosts.count > 0 {
            let post = self.arrPosts[indexPath.row]
            postBody = post.body?.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil) ?? ""
            cell.lblTitle.text = post.title
            cell.lblDes.text = postBody

        }else{
            let post = self.arrPostdata[indexPath.row]
            postBody = post.body?.replacingOccurrences(of: "\n", with: " ", options: .literal, range: nil) ?? ""
            cell.lblTitle.text = post.title
            cell.lblDes.text = postBody
            
        }
        
        return cell


    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        
        var desc = "Feeling happy"
        var strTitle = "Test"
        if arrPosts.count > 0 {
            let post = self.arrPosts[indexPath.row]
            desc = post.body ?? "Feeling happy"
            strTitle = post.title ?? ""

        }else{
            let post = self.arrPostdata[indexPath.row]
            desc = post.body ?? "Feeling happy"
            strTitle = post.title ?? ""

        }

        let approximatewidth = collectionView.frame.width-60
        let size = CGSize(width: approximatewidth, height: 400)
        let attribute = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]
    
        let estimateFrame = NSString(string: desc).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attribute, context: nil)
        let attributetitle = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)]

        let estimateFrameSize = NSString(string: strTitle).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributetitle, context: nil)

        return CGSize(width: self.collectionView.frame.width, height: estimateFrame.height + estimateFrameSize.height + 35)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "postdetail") as! PostDetailVC

        if self.arrPosts.count > 0{
            let post = self.arrPosts[indexPath.row]
            vc.strPostTitle = post.title ?? ""
            vc.strPostDesc = post.body ?? ""
            vc.userid = Int(post.userId )
            vc.postId = Int(post.id )

        }else{
            let post = self.arrPostdata[indexPath.row]
            vc.strPostTitle = post.title ?? ""
            vc.strPostDesc = post.body ?? ""
            vc.userid = post.userId ?? 1
            vc.postId = post.id ?? 1

        }

        self.navigationController?.pushViewController(vc, animated: true)

    }

}
