//
//  CoreDataManager.swift
//  ConsumerSketch
//
//  Created by Consumer Sketch  on 01/04/21.
//  Copyright © 2021 Daksh Bhagat. All right

import Foundation
import CoreData

class CoreDataManager: NSObject{
    
    private override init() {
        super.init()
        
        applicationLibraryDirectory()
    }
    // Create a shared Instance
    static let _shared = CoreDataManager()
    
    // Shared Function
    class func shared() -> CoreDataManager{
        return _shared
    }
    
    // Get the location where the core data DB is stored
    
    private lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print(urls[urls.count-1])
        return urls[urls.count-1]
    }()
    
    private func applicationLibraryDirectory() {
        print(applicationDocumentsDirectory)
        if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
            print(url.absoluteString)
        }
    }
    // MARK: - Core Data stack
    
    // Get the managed Object Context
    lazy var managedObjectContext = {
        
        return self.persistentContainer.viewContext
    }()
    // Persistent Container
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "ConsumerSketch")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func resetAllRecords(in entity : String) // entity = Your_Entity_Name
    {

        let context = self.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do
        {
            try context.execute(deleteRequest)
            try context.save()
        }
        catch
        {
            print ("There was an error")
        }
    }

    func preparePostList(dataForSaving: [PostViewModel]){
        
        // loop through all the data received from the Web and then convert to managed object and save them
        _ = dataForSaving.map{self.createEntityFromPost(posts: $0)}
        saveData()
    }
    
    private func createEntityFromPost(posts: PostViewModel) -> Posts?{
        
        let id = posts.id
        let userId = posts.userId
        // Check for all values
        guard let title = posts.title,let body = posts.body else {
            
            return nil
        }
        // Convert
        let postItems = Posts(context: self.managedObjectContext)

        postItems.body = body
        postItems.id = Int16(id!) as Int16
        postItems.title = title
        postItems.userId = Int16(userId!) as Int16
        
        return postItems
        
        
    }
    
    
    func prepareUserList(dataForSaving: [UserViewModel]){
        
        // loop through all the data received from the Web and then convert to managed object and save them
        _ = dataForSaving.map{self.createEntityFromUser(user: $0)}
        saveData()
    }
    
    private func createEntityFromUser(user: UserViewModel) -> Users?{
        

        let id = user.id
        let street = user.address?["street"] as? String
        let suite = user.address?["suite"] as? String
        let add = "\(street ?? "") \(suite ?? "")"

        // Check for all values
        guard let name = user.name,let email = user.email,let phone = user.phone,let username = user.username,let     website = user.website else {
            
            return nil
        }
        // Convert
        let userItem = Users(context: self.managedObjectContext)

        userItem.name = name
        userItem.id = Int16(id!) as Int16
        userItem.email = email
        userItem.phone = phone
        userItem.website = website
        userItem.username = username
        userItem.address = add

        
        return userItem
        
        
    }
    
    
    func prepareCommentList(dataForSaving: [CommentViewModel]){
        
        // loop through all the data received from the Web and then convert to managed object and save them
        _ = dataForSaving.map{self.createEntityFromComment(comment: $0)}
        saveData()
    }
    
    private func createEntityFromComment(comment: CommentViewModel) -> Comments?{
        
        let id = comment.id
        let postId = comment.postId
        // Check for all values
        guard let name = comment.name,let body = comment.body,let email = comment.email else {
            
            return nil
        }
        // Convert
        let commentItem = Comments(context: self.managedObjectContext)

        commentItem.name = name
        commentItem.id = Int16(id ?? 0) as Int16
        commentItem.body = body
        if let postId = postId {
            commentItem.postId = (Int16(postId ) ?? 0) as Int16

        }
        commentItem.email = email

        return commentItem
        
        
    }
    
    // Save the data in Database
    func saveData(){
        
        let context = self.managedObjectContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // Save Data in background
    func saveDataInBackground() {
        
        persistentContainer.performBackgroundTask { (context) in
            
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }
}
