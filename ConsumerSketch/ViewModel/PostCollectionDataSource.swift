//
//  PostCollectionDataSource.swift
//  ConsumerSketch
//
//  Created by Tarun Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

//import Foundation
//import UIKit
//
//class PostCollectionDataSource<CELL : UICollectionViewCell,T> : NSObject, UICollectionViewDataSource {
//    
//    private var cellIdentifier : String!
//    private var items : [T]!
//    var configureCell : (CELL, T) -> () = {_,_ in }
//    
//    
//    init(cellIdentifier : String, items : [T], configureCell : @escaping (CELL, T) -> ()) {
//        self.cellIdentifier = cellIdentifier
//        self.items =  items
//        self.configureCell = configureCell
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        items.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//     
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postcell", for: indexPath) as! PostCell
//        cell.imgPost.layer.cornerRadius = 25
//        let post = self.items[indexPath.row]
//        cell.lblName.text = post.body
//        return cell
//        
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//         let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CELL
//        
//        let item = self.items[indexPath.row]
//        self.configureCell(cell, item)
//        return cell
//    }
//}
