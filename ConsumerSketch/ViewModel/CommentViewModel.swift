//
//  CommentViewModel.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class CommentViewModel: Decodable {

    let id : Int?
    let postId : Int?
    let name : String?
    let email : String?
    let body : String?


    init(comment: CommentModel){
        
        self.id = comment.id
        self.postId = comment.postId
        self.name = comment.name
        self.email = comment.email
        self.body = comment.body

    }
}
