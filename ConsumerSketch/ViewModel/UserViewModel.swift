//
//  UserViewModel.swift
//  ConsumerSketch
//
//  Created by Daksh Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class UserViewModel{

    let id : Int?
    let name : String?
    let username : String?
    let email : String?
    let phone : String?
    let website : String?
    let address: [String : Any]?
    let company: [CompanyModel]?


    init(user: UserModel){
        self.id = user.id
        self.name = user.name
        self.username = user.username
        self.email = user.email
        self.phone = user.phone
        self.website = user.website
        self.address = user.address
        self.company = user.company


    }
}

class AddressViewModel: Decodable {
    
    let street : String?
    let suite : String?
    let city : String?
    let zipcode : String?
    
    init(address: AddressModel){
        
        self.street = address.street
        self.suite = address.suite
        self.city = address.city
        self.zipcode = address.zipcode

    }
}

class CompanyViewModel: Decodable {
    
    let name : Int?
    let catchPhrase : String?
    let bc : String?

    init(company: CompanyModel){
        
        self.name = company.name
        self.catchPhrase = company.catchPhrase
        self.bc = company.bc

    }
}


