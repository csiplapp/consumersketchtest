//
//  PostViewModel.swift
//  ConsumerSketch
//
//  Created by Daksj Bhagat on 31/03/21.
//  Copyright © 2021 Daksh Bhagat. All rights reserved.
//

import UIKit

class PostViewModel{

    var id : Int?
    var userId : Int?
    var title : String?
    var body : String?


    init(post: PostModel){
        
        self.id = post.id
        self.userId = post.userId
        self.title = post.title
        self.body = post.body

    }
}
